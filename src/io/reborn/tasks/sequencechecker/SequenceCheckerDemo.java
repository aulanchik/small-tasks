package io.reborn.tasks.sequencechecker;

import io.reborn.tasks.sequencechecker.utils.SequenceCheckHelper;

public class SequenceCheckerDemo {

    private static int[] INPUT = new int[] {1,2,3,3,3,3,3,3,3,3,4,5,5};

    public static void main(String[] args) {
        int result = SequenceCheckHelper.findRecurringValue(INPUT);
        System.out.println("Longest recurring value is " + result);
    }
}
