package io.reborn.tasks.sequencechecker.utils;

public final class SequenceCheckHelper {

    public static int findRecurringValue(int[] inputData) {

        int previous = inputData[0];
        int recurring = inputData[0];
        int counter = 1;
        int maxCounter = 1;

        for (int i = 1; i < inputData.length; i++) {
            if (inputData[i] == previous) {
                counter++;
            } else {
                if (counter > maxCounter) {
                    recurring = inputData[i - 1];
                    maxCounter = counter;
                }
                previous = inputData[i];
                counter = 1;
            }
        }
        return counter > maxCounter ? inputData[inputData.length - 1] : recurring;
    }
}
