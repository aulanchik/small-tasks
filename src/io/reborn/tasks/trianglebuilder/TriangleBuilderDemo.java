package io.reborn.tasks.trianglebuilder;

import io.reborn.tasks.trianglebuilder.utils.TriangleBuilder;

import java.util.InputMismatchException;
import java.util.Scanner;

public class TriangleBuilderDemo {

    private static String SYMBOL = "*";
    private static String DELIMITER = " ";

    public static void main(String[] args) {

        int baseNumber = -1;
        Scanner scanner = new Scanner(System.in);
        while (baseNumber < 0) {
            try{
                System.out.print("Input base number of desired ASCII triangle: ");
                baseNumber = scanner.nextInt();
            }catch (InputMismatchException e) {
                scanner.next();
            }
        }

        TriangleBuilder.drawTriangle(SYMBOL, DELIMITER, baseNumber);
    }
}
