package io.reborn.tasks.trianglebuilder.utils;

public final class TriangleBuilder {

    public static void drawTriangle(String symbol, String delimiter, int base) {
        System.out.println();
        for (int i = 0; i <= base; i += 2){
            for (int step = 0; step < (base - i / 2); step++){
                System.out.print(delimiter);
            }
            for (int j = 0; j <= i; j++){
                System.out.print(symbol);
            }
            System.out.println();
        }
    }
}
